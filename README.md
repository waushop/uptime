# Uptime Monitor

 Monitor is a web application that shows project's availability status on a timeline with predescribed timeframe.

## Getting Started

##### `git clone https://waushop@bitbucket.org/waushop/uptime.git`

Clone the project to your projects directory.

##### `yarn install`

Install UI dependencies.

##### `yarn start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

##### `yarn build`

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment


## Built With

* [ReactJS](reactjs.org/) - A JavaScript library for building user interfaces
* [React Animation](https://nearform.github.io/react-animation//) - React animations Library
* [React Tooltip](https://wwayne.github.io/react-tooltip//) - React tooltip library
* [React Loader Spinner](https://wwayne.github.io/react-tooltip//) - Simple React SVG spinner component
* [DayJS](https://day.js.org/) - JavaScript date utility library

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details