import React from 'react';
import ReactTooltip from 'react-tooltip';
import MonitorBlock from './monitor-block';

const MonitorItem = (props) => {

  const { project } = props;
    
  const status = {
    up: 'Available',
    semidown: 'Partially Unavailable',
    down: 'Unavailable',
  };

  return (
    <div className="item">
      <div className="head">
        <div className="info">
          <span className="name">{project.name}</span>
        </div>
        <div className={`status ${project.status}`}>{status[project.status]}</div>
      </div>
      <div className="time">
        {project.daily.map((value, i) => (
          <MonitorBlock key={i} data={value} />
        ))}
      </div>
      <ReactTooltip className="tooltip" place="bottom" type="light" effect="float" />
      <div className="foot">
        <span>72 hours ago</span>
        <span>Total downtime: {project.totalDowntime} min  </span>
        <span>Today</span>
      </div>
    </div>
  );
}

export default MonitorItem;
