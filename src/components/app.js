import React from 'react';
import { animations } from 'react-animation'
import Monitor from './monitor';

const App = () => {
  
  return (
    <>
      <div className="text-center">
        <h1 style={{animation: animations.bounceIn}}>Uptime Monitor</h1>
      </div>
      <div style={{animation: animations.fadeIn}} className="container">
        <div className="" id="monitor">
          <Monitor />
        </div>
      </div>
    </>
  );
}

export default App;
