import React, { useMemo } from 'react';

const MonitorBlock = (props) => {

  const { data } = props;
  const { status, text } = useMemo(() => {
    let status = '';

    //Date and time formatting
    let text = data.date.format('HH:mm:ss (Z) / DDMMMYYYY');

    //Tooltip showing date and time when hovering timeline bar
    //and showing project's downtime in certain timeframe
    if (data.monitor >= 60) {
      status = 'up';
    }
    else if (data.monitor <= 0) {
      status = 'down';
      text += ` DOWNTIME: ${60 - data.monitor}min`;
    }
    else {
      status = 'semidown';
      text += ` DOWNTIME: ${60 - data.monitor}min`;
    }
    return { status, text };
  }, [data]);

  return (
    <i className={status} data-tip={text}></i>
  );
}

export default MonitorBlock;