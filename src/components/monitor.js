import React, { useEffect, useState } from 'react';
import Loader from 'react-loader-spinner'
import MonitorItem from './monitor-item';
import { Projects } from '../utils/monitordata';

// eslint-disable-next-line 
import { bars } from '../utils/config';

const UptimeMonitor = () => {
  const { bars } = window.settings;
  const [projects, setProjects] = useState(null);

  useEffect(() => {
    Projects(bars).then(setProjects);
  }, [bars]);

  return projects ? projects.map(item => (
    <MonitorItem key={item.id} project={item} />
  )) : <Loader type="Audio" color="#00BFFF" height={50} width={50} />
  ;
}

export default UptimeMonitor;


