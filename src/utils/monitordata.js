import dayjs from 'dayjs';

export const Projects = async (days) => {

  const dates = [];
  const today = dayjs(new Date());
  for (let d = 0; d < days; d++) {
    dates.push(today.subtract(d, 'hours'));
  }
  
  // Demo data ...
  const projects = [{
    id: 1,
    name: "Project Alfa",
    monitor_ranges: "60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60",
    status: 2
    },
    {
    name: "Project Bravo",
    id: 2,
    monitor_ranges: "60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-40-40-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60",
    status: 2
    },
    {
    name: "Project Charlie",
    id: 3,
    monitor_ranges: "30-20-10-0-0-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-37-39-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-40-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60",
    status: 1
    },
    {
    name: "Project Delta",
    id: 4,
    monitor_ranges: "0-0-0-0-10-20-30-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-11-0-14-1-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-30-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60-60",
    status: 0
    }  
  ]

  const data = [];
  projects.forEach(project => {
    let ranges = project.monitor_ranges.split('-');
    let daily = [];

    dates.forEach((date, i) => {
      daily[i] = {
        date: date,
        monitor: ranges[i],
      }
    });
    
    // Total downtime ...
    const nums = ranges.toString().split(',').map(x => parseInt(x, 10));
    let totalUptime = 0
    let totalDowntime = 0

    totalUptime = nums.reduce((a, b) => a + b, 0);

    totalDowntime = (73 * 60) - totalUptime


   
    // Reverse daily array ...
    daily.reverse();

    //Project overall availability depending on the most recent availability status ...
    let status = 'up';
    if (daily[71].monitor > 0 && daily[71].monitor < 60) status = 'semidown';
    if (daily[71].monitor <= 0) status = 'down';

    data.push({
      id: project.id,
      name: project.name,
      status: status,
      daily: daily,
      totalDowntime : totalDowntime
    });
  });
  return Promise.resolve(data);
}